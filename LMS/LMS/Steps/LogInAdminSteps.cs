﻿using LMS.POM;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using TechTalk.SpecFlow;

namespace LMS.Steps
{
    [Binding]
    public class LogInAdminSteps: Initialization
    {
       
        public MainPage mainPage;
        public ProfessionPage ProfPage;
     
        [Given(@"User is on Authorization Page")]
        public void GivenUserIsOnAuthorizationPage()
        {
            OpenDriver();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(120);
            driver.Navigate().GoToUrl("https://course-platform.stage-realpro.com/login");
            driver.Manage().Window.Maximize();
            mainPage = new MainPage(driver);
            ProfPage = new ProfessionPage(driver);
            string AuthorizationPageLabelText = mainPage.GetTextFromAuthorizationPage();
            Assert.AreEqual("Авторизация", AuthorizationPageLabelText);

        }

        [When(@"User fills ""(.*)"" in email field")]
        public void WhenUserFillsInEmailField(string email)
        {
            mainPage.EnterEmail(email);
        }

        [When(@"User fills ""(.*)"" in the password field")]
        public void WhenUserFillsInThePasswordField(string password)
        {
            mainPage.EnterPassword(password);
        }
        
        [When(@"User presses Log In Button")]
        public void WhenUserPressesLogInButton()
        {
            mainPage.ClickOnEnterBtn();
        }
        
        [Then(@"Profession page is open")]
        public void ThenProfessionPageIsOpen()
        {
            string ProfessionPageLabelText = ProfPage.GetTextFromProfessionPage();
            Assert.AreEqual("Список профессий", ProfessionPageLabelText);
        }

        
        [When(@"Close the window")]
        public void WhenCloseTheWindow()
        {
            WhenCloseWindow();
        }


    }
}
