﻿using LMS.POM;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using TechTalk.SpecFlow;

namespace LMS.Steps
{
    [Binding]
    public class ForgetPasswordSteps:Initialization
    {
       
        public MainPage mainPage;
        public ForgetPassword forgetPass;



        [Given(@"Course-platform page is open")]
        public void GivenCourse_PlatformPageIsOpen()
        {
            OpenDriver();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(120);
            driver.Navigate().GoToUrl("https://course-platform.stage-realpro.com/login");
            driver.Manage().Window.Maximize();
            mainPage = new MainPage(driver);
            forgetPass = new ForgetPassword(driver);
            
        }
        
        [When(@"User clicks on Forget password button")]
        public void WhenUserClicksOnForgetPasswordButton()
        {
            mainPage.ClickOnForgetPassBtn();
            
        }
        
        [Then(@"Forgot password page is open")]
        public void ThenForgotPasswordPageIsOpen()
        {
            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(15);
            string RecoveryPassText = forgetPass.GetTextFromForgetPasswordl();
            Assert.AreEqual("Восстановление пароля", RecoveryPassText);
        }

        [When(@"Close window")]
        public void CloseWindow()
        {
            WhenCloseWindow();
        }

    }
}
