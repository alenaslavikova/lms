﻿using LMS.POM;
using NUnit.Framework;
using System;
using TechTalk.SpecFlow;

namespace LMS.Steps
{
    [Binding]
    public class MyProfileSteps
    {

        public MyProfilePage myProfilePage;
        public MyProfilePopUps myProfilePopUps;
        public ProfessionPage professionPage;

        [When(@"User clicks on My Profile Button")]
        public void WhenUserClicksOnMyProfileButton()
        {
            professionPage.ClickOnMyProfileIcon();
        }

        [When(@"Profile Page is opened")]
        public void WhenProfilePageIsOpened()
        {
            string MyProfileLabelText = myProfilePage.GetTextFromMyProfilePage();
            Assert.AreEqual("Мой профиль", MyProfileLabelText);
        }

        [When(@"User clicks on FirstName field")]
        public void WhenUserClicksOnFirstNameField()
        {
            myProfilePage.ClickOnFirstNameBtn();
        }

        [When(@"Edit Pop Up name is shown")]
        public void WhenEditPopUpNameIsShown()
        {
             string FirstNamePopUpLabelText = myProfilePopUps.GetTextFromFirstName();
             Assert.AreEqual("Имя", FirstNamePopUpLabelText);
            
        }

        [When(@"User fills ""(.*)"" in name field")]
        public void WhenUserFillsInNameField(string name)
        {
            myProfilePopUps.EnterFirstName(name);
        }


        [When(@"User clicks on Save button")]
        public void WhenUserClicksOnSaveButton()
        {
            myProfilePopUps.ClickOnSaveBtn();
        }
        
        
        [Then(@"User sees edited name")]
        public void ThenUserSeesEditedName()
        {
            string EditedFirstNameLabelText = myProfilePage.GetTextFromEditedFirstName();
            Assert.AreEqual("Arina", EditedFirstNameLabelText);

        }
    }
}
