﻿Feature: My profile
	As an Administrator I want to be able to edit personal data 
	In order to perform up-to-date information

Background: 
   Given User is on Authorization Page
	When  User fills "platformcourse1@gmail.com" in email field
	When User fills "qwerty1234" in the password field
	When User presses Log In Button 
	Then Profession page is open



Scenario: Edit My Profile Information and chaange my First Name 
	When User clicks on My Profile Button
	When Profile Page is opened
	When User clicks on FirstName field
	When Edit Pop Up name is shown
	When User fills "Arina" in name field
	When User clicks on Save button
	Then User sees edited name

