﻿Feature: ForgetPassword
	As a user I want to be able to recover my password in case I forgot it 
	in order to have access to my account


Scenario: Send recovery password request

	Given Course-platform page is open
	When User clicks on Forget password button
	Then Forgot password page is open
	When Close window