﻿Feature: LogInAdmin
	As an Administrator 
	I want to log in to system so that I could control the work of application and perform changes

@Authorization
Scenario: Log in with admin's credentials
	Given User is on Authorization Page
	When  User fills "platformcourse1@gmail.com" in email field
	When User fills "qwerty1234" in the password field
	When User presses Log In Button 
	Then Profession page is open
	When Close the window
