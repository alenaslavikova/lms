﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.POM
{
    public class MainPage
    {
        private IWebDriver _driver;


        public By AuthorizationTitle = By.XPath("/html/body/div/div[2]/div[2]/div/h1");
        public By LogInBtn = By.XPath("/html/body/div/div[2]/div[2]/div/div/form/button");
        public By EmailField = By.XPath("/html/body/div/div[2]/div[2]/div/div/form/div[1]/input");
        public By PasswordField = By.XPath("/html/body/div/div[2]/div[2]/div/div/form/div[2]/input");
        public By ForgetPassword = By.XPath("/html/body/div/div[2]/div[2]/div/div/a");
        
        public MainPage(IWebDriver driver)
        {
            this._driver = driver;

        }

        public IWebElement FindForgetPassword()
        {
            return _driver.FindElement(ForgetPassword);
        }


        public ForgetPassword ClickOnForgetPassBtn()
        {
            _driver.FindElement(ForgetPassword).Click();
            return new ForgetPassword(_driver);
        }

        public MainPage EnterEmail(string email)
        {
            _driver.FindElement(EmailField).SendKeys(email);
            return new MainPage(_driver);
        }

        public MainPage EnterPassword(string password)
        {
            _driver.FindElement(PasswordField).SendKeys(password);
            return new MainPage(_driver);
        }

        public ProfessionPage ClickOnEnterBtn()
        {
            _driver.FindElement(LogInBtn).Click();
            return new ProfessionPage(_driver);
        }

        public IWebElement FindAuthorizationTitle()
        {
            return _driver.FindElement(AuthorizationTitle);
        }
        public string GetTextFromAuthorizationPage()
        {
            return _driver.FindElement(AuthorizationTitle).Text;
        }

    }

}
