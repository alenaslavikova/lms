﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.POM
{
    public class MyProfilePopUps
    {
        private IWebDriver _driver;
        public MyProfilePopUps(IWebDriver driver)
        {
            this._driver = driver;

        }

        public By FirstNameTitle = By.XPath("/html/body/div[1]/div[4]/div/div[2]/div[3]/form/label");
        public By FirstNameInput = By.XPath("/html/body/div[1]/div[4]/div/div[2]/div[3]/form/div[1]/input");
        public By SaveBtn = By.XPath("/html/body/div[1]/div[4]/div/div[2]/div[3]/form/div[2]/button[2]");
        public By CancelBtn = By.XPath("/html/body/div[1]/div[4]/div/div[2]/div[3]/form/div[2]/button[1]");

        public string GetTextFromFirstName()
        {
            return _driver.FindElement(FirstNameTitle).Text;
        }

        //public string GetTextFromFirstNameInput()
        //{
        //    return _driver.FindElement(FirstNameInput).Text;
        //}

        public MyProfilePopUps EnterFirstName(string name)
        {
            _driver.FindElement(FirstNameInput).SendKeys(name);
            return new MyProfilePopUps(_driver);
        }

        public MyProfilePage ClickOnSaveBtn()
        {
            _driver.FindElement(SaveBtn).Click();
            return new MyProfilePage(_driver);
        }

        public MyProfilePage ClickOnCancelBtn()
        {
            _driver.FindElement(CancelBtn).Click();
            return new MyProfilePage(_driver); 
        }


    }
}
