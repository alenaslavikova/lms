﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.POM
{
    public class ForgetPassword
    {
        private IWebDriver _driver;

        public ForgetPassword (IWebDriver driver)
        {
            this._driver = driver;

        }

        public By PasswordRecovery = By.XPath("/html/body/div/div[2]/div[2]/div/h1");


        
        public string GetTextFromForgetPasswordl()
        {
            return _driver.FindElement(PasswordRecovery).Text;
        }




    }
}
