﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.POM
{
    public class MyProfilePage
    {
        private IWebDriver _driver;

        public By MyProfileTitle = By.XPath("/html/body/div/div[4]/div/div[1]");
        public By FirstName = By.XPath("/html/body/div/div[4]/div/div[2]/div[1]");
        public By LastName = By.XPath("/html/body/div/div[4]/div/div[2]/div[2]");
        public By Phone = By.XPath("/html/body/div/div[4]/div/div[2]/div[4]");
        public By Country = By.XPath("/html/body/div/div[4]/div/div[2]/div[5]");
        public By City = By.XPath("/html/body/div/div[4]/div/div[2]/div[6]");
        public By Gender = By.XPath("/html/body/div/div[4]/div/div[2]/div[8]");
        public By EditedFirstNameTitle = By.XPath("/html/body/div[1]/div[4]/div/div[2]/div[1]/div[1]");
        public MyProfilePage(IWebDriver driver)
        {
            this._driver = driver;
        }

        public string GetTextFromMyProfilePage()
        {
            return _driver.FindElement(MyProfileTitle).Text;
        }

        public IWebElement FindFirstName()
        {
            return _driver.FindElement(FirstName);
        }

        public MyProfilePopUps ClickOnFirstNameBtn()
        {
            _driver.FindElement(FirstName).Click();
            return new MyProfilePopUps(_driver);
        }


        public string GetTextFromEditedFirstName()
        {
            return _driver.FindElement(EditedFirstNameTitle).Text;
        }

    }
}
