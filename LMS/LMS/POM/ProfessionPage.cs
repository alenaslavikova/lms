﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.POM
{
    public class ProfessionPage
    {
        private IWebDriver _driver;
        public ProfessionPage (IWebDriver driver)
        {
            this._driver = driver;

        }

        public By ProfessionPageTitle = By.XPath("/html/body/div/div[4]/div/div[2]");
        public By MyProfileIcon = By.XPath("/html/body/div[1]/div[2]/div/div[2]/div[1]");
        public string GetTextFromProfessionPage()
        {
            return _driver.FindElement(ProfessionPageTitle).Text;
        }
        public IWebElement FindMyProfileIcon()
        {
            return _driver.FindElement(MyProfileIcon);
        }

        public MyProfilePage ClickOnMyProfileIcon()
        {
            _driver.FindElement(MyProfileIcon).Click();
            return new MyProfilePage(_driver);
        }

    }
}
