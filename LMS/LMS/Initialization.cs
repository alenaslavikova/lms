﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS
{
    public class Initialization
    {
        public IWebDriver driver;
        public void OpenDriver()
        {
            driver = new ChromeDriver(@"C:\chromedriver_win32 (1)");
            //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(120);
            //driver.Navigate().GoToUrl("https://course-platform.stage-realpro.com/login");
            //driver.Manage().Window.Maximize();

        }

        public void WhenCloseWindow()
        {
            driver.Dispose();
        }
    }
}
